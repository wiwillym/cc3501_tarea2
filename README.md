Para ingresar puntos se debe modificar el archivo "trayectoria.txt", el cual tendra el siguiente formato como ejemplo:
	
	250,0
	250,10
	250,50
	500,100
	500,300
	400,450
	250,425
	100,400
	100,100
	
donde cada línea especifica un punto x,y en pixeles, luego se debe ejecutar el programa "navegacion.py".
Cabe mencionar que la ventana es de 800x600 pixeles, y que además todos los "import" estan basados en el computador en el que trabajé, 
por lo que se podría necesitar cambiarlos.