from OpenGL.GL import *
import numpy as np
import math
from utils3d import *
filename = open("trayectoria3d.txt", "r")
Pauxstr = []
for line in filename:
    Pauxstr.append(line)
Pauxstr[len(Pauxstr) - 1] += '\n'
Points = txttovector(Pauxstr)


class Boat:
    def __init__(self, p):
        self.p = np.array(p) #  posicion
        self.trayectoria = self.calcularTrayectoria(Points)

    def calcularTrayectoria(self, P):
        CRS = CatmullRomChain([P[0], P[1], P[2], P[3]])
        for p in range(len(P)):
            if p + 4 <= len(P) - 1:
                Paux = [P[p + 1], P[p + 2], P[p + 3], P[p + 4]]
                CRSaux = CatmullRomChain(Paux)
                CRS = np.concatenate((CRS, CRSaux))
            else:
                break
        return CRS

    def mover(self, instante, maxInstante):
        self.calcularTrayectoria(Points)
        self.p = np.array([self.trayectoria[instante][0], self.trayectoria[instante][1]])

    def dibujar(self, y2, y1, x2, x1):

        glPushMatrix()

        glTranslatef(self.p[0], self.p[1], 0.0)

        glRotatef(math.degrees(np.arctan((y2 - y1) / (x2 - x1))), 0, 0, 1)

        #   Base n_1
        glMaterialfv(GL_FRONT, GL_AMBIENT, [0.10, 0.048, 0.06475])
        glMaterialfv(GL_FRONT, GL_DIFFUSE, [0.4, 0.2368, 0.1036])
        glMaterialfv(GL_FRONT, GL_SPECULAR, [0.774597, 0.458561, 0.200621])
        glMaterialfv(GL_FRONT, GL_SHININESS, [76.8])

        glBegin(GL_POLYGON)

        glVertex3fv([-40, 0, 15])
        glVertex3fv([-25, -25, 15])
        glVertex3fv([25, -25, 15])
        glVertex3fv([40, 0, 15])
        glVertex3fv([25, 25, 15])
        glVertex3fv([-25, 25, 15])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-25, -15, 0])
        glVertex3fv([-25, 15, 0])
        glVertex3fv([25, 15, 0])
        glVertex3fv([25, -15, 0])

        glEnd()

        glBegin(GL_POLYGON)

        glVertex3fv([-25, -15, 0])
        glVertex3fv([25, -15, 0])
        glVertex3fv([25, -25, 15])
        glVertex3fv([-25, -25, 15])

        glEnd()

        glBegin(GL_POLYGON)

        glVertex3fv([-25, 15, 0])
        glVertex3fv([25, 15, 0])
        glVertex3fv([25, 25, 15])
        glVertex3fv([-25, 25, 15])

        glEnd()

        glBegin(GL_TRIANGLES)

        glVertex3fv([-30, 0, 0])
        glVertex3fv([-25, 15, 0])
        glVertex3fv([-25, -15, 0])

        glEnd()

        glBegin(GL_TRIANGLES)

        glVertex3fv([30, 0, 0])
        glVertex3fv([25, 15, 0])
        glVertex3fv([25, -15, 0])

        glEnd()

        glBegin(GL_POLYGON)

        glVertex3fv([30, 0, 0])
        glVertex3fv([40, 0, 15])
        glVertex3fv([25, 25, 15])
        glVertex3fv([25, 15, 0])

        glEnd()

        glBegin(GL_POLYGON)

        glVertex3fv([30, 0, 0])
        glVertex3fv([40, 0, 15])
        glVertex3fv([25, -25, 15])
        glVertex3fv([25, -15, 0])

        glEnd()

        glBegin(GL_POLYGON)

        glVertex3fv([-30, 0, 0])
        glVertex3fv([-40, 0, 15])
        glVertex3fv([-25, 25, 15])
        glVertex3fv([-25, 15, 0])

        glEnd()

        glBegin(GL_POLYGON)

        glVertex3fv([-30, 0, 0])
        glVertex3fv([-40, 0, 15])
        glVertex3fv([-25, -25, 15])
        glVertex3fv([-25, -15, 0])

        glEnd()

        #   Base n_2
        #glColor3fv(rgb(102, 51, 0))
        glMaterialfv(GL_FRONT, GL_AMBIENT, [0.2125, 0.1275, 0.054])
        glMaterialfv(GL_FRONT, GL_DIFFUSE, [0.714, 0.4284, 0.18144])
        glMaterialfv(GL_FRONT, GL_SPECULAR, [0.393548, 0.271906, 0.166721])
        glMaterialfv(GL_FRONT, GL_SHININESS, [25.6])

        glBegin(GL_QUADS)

        glVertex3fv([-22, -12, 0])
        glVertex3fv([-22, 12, 0])
        glVertex3fv([22, 12, 0])
        glVertex3fv([22, -12, 0])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-22, -12, 15])
        glVertex3fv([-22, 12, 15])
        glVertex3fv([22, 12, 15])
        glVertex3fv([22, -12, 15])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-22, -12, 0])
        glVertex3fv([-22, -12, 15])
        glVertex3fv([22, -12, 15])
        glVertex3fv([22, -12, 0])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-22, 12, 0])
        glVertex3fv([-22, 12, 15])
        glVertex3fv([22, 12, 15])
        glVertex3fv([22, 12, 0])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-22, -12, 0])
        glVertex3fv([-22, -12, 15])
        glVertex3fv([-22, 12, 15])
        glVertex3fv([-22, 12, 0])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([22, -12, 0])
        glVertex3fv([22, -12, 15])
        glVertex3fv([22, 12, 15])
        glVertex3fv([22, 12, 0])

        glEnd()

        #   Sails
        #glColor3fv(rgb(255, 255, 255))
        glMaterialfv(GL_FRONT, GL_AMBIENT, [0.0, 0.0, 0.0])
        glMaterialfv(GL_FRONT, GL_DIFFUSE, [0.55, 0.55, 0.55])
        glMaterialfv(GL_FRONT, GL_SPECULAR, [0.70, 0.70, 0.70, 0.70])
        glMaterialfv(GL_FRONT, GL_SHININESS, [0.25])
        #   Sail_1
        glBegin(GL_TRIANGLES)

        glVertex3fv([3, -2, 30])
        glVertex3fv([3, 0, 98])
        glVertex3fv([30, 0, 30])

        glEnd()

        glBegin(GL_TRIANGLES)

        glVertex3fv([3, 2, 30])
        glVertex3fv([3, 0, 98])
        glVertex3fv([30, 0, 30])

        glEnd()

        #   Sail_2
        glColor3fv(rgb(255, 255, 255))

        glBegin(GL_TRIANGLES)

        glVertex3fv([-3, -2, 30])
        glVertex3fv([-3, 0, 98])
        glVertex3fv([-30, 0, 30])

        glEnd()

        glBegin(GL_TRIANGLES)

        glVertex3fv([-3, 2, 30])
        glVertex3fv([-3, 0, 98])
        glVertex3fv([-30, 0, 30])

        glEnd()

        #   Stick

        #glColor3fv(rgb(102, 51, 0))
        glMaterialfv(GL_FRONT, GL_AMBIENT, [0.2125, 0.1275, 0.054])
        glMaterialfv(GL_FRONT, GL_DIFFUSE, [0.714, 0.4284, 0.18144])
        glMaterialfv(GL_FRONT, GL_SPECULAR, [0.393548, 0.271906, 0.166721])
        glMaterialfv(GL_FRONT, GL_SHININESS, [25.6])

        glBegin(GL_QUADS)

        glVertex3fv([-3, -3, 15])
        glVertex3fv([-3, -3, 100])
        glVertex3fv([-3, 3, 100])
        glVertex3fv([-3, 3, 15])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([3, 3, 15])
        glVertex3fv([3, 3, 100])
        glVertex3fv([3, -3, 100])
        glVertex3fv([3, -3, 15])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-3, -3, 15])
        glVertex3fv([-3, -3, 100])
        glVertex3fv([3, -3, 100])
        glVertex3fv([3, -3, 15])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-3, 3, 15])
        glVertex3fv([-3, 3, 100])
        glVertex3fv([3, 3, 100])
        glVertex3fv([3, 3, 15])

        glEnd()

        glBegin(GL_QUADS)

        glVertex3fv([-3, -3, 100])
        glVertex3fv([-3, 3, 100])
        glVertex3fv([3, 3, 100])
        glVertex3fv([3, -3, 100])

        glEnd()

        glPopMatrix()
