import numpy as np
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import matplotlib.pyplot as plt
import keyboard


def rgb(r, g, b):
    return [r/255.0, g/255.0, b/255.0]


def CatmullRomSpline(P0, P1, P2, P3, nPoints=100):

    #   Gracias a Wikipedia por hacerme entender el algoritmo para los Splines de Catmull-Rom

    alpha = 0.5

    def tj(ti, Pi, Pj):
        xi, yi = Pi
        xj, yj = Pj
        return (((xj - xi)**2 + (yj - yi)**2)**0.5)**alpha + ti

    t0 = 0
    t1 = tj(t0, P0, P1)
    t2 = tj(t1, P1, P2)
    t3 = tj(t2, P2, P3)

    #   Calculates only for points between P1 and P2
    t = np.linspace(t1, t2, nPoints)

    # Reshape so that we can multiply by the points P0 to P3
    # and get a point for each value of t.
    t = t.reshape(len(t), 1)

    A1 = (t1 - t)/(t1 - t0)*P0 + (t - t0)/(t1 - t0)*P1
    A2 = (t2 - t)/(t2 - t1)*P1 + (t - t1)/(t2 - t1)*P2
    A3 = (t3 - t)/(t3 - t2)*P2 + (t - t2)/(t3 - t2)*P3

    B1 = (t2 - t)/(t2 - t0)*A1 + (t - t0)/(t2 - t0)*A2
    B2 = (t3 - t)/(t3 - t1)*A2 + (t - t1)/(t3 - t1)*A3

    C = (t2 - t)/(t2 - t1)*B1 + (t - t1)/(t2 - t1)*B2

    return C


def CatmullRomChain(P):

    z = len(P)

    C = []
    for i in range(z - 3):
        c = CatmullRomSpline(P[i], P[i + 1], P[i + 2], P[i + 3])
        C.extend(c)

    return C


def DrawCRS(CRS, P):

    for p in range(len(CRS) - 1):

        aux = CRS[p]
        aux2 = CRS[p + 1]

        glLineWidth(2.5)

        glBegin(GL_LINES)
        glVertex2f(aux[0], aux[1])
        glVertex2f(aux2[0], aux2[1])
        glEnd()


def SpaceDrawing(CRS, P):
    if keyboard.is_pressed('space'):
        DrawCRS(CRS, P)


def txttovector(Paux2str):

    Paux2 = []

    for str in Paux2str:
        tupla = (0, 0)
        lst = list(tupla)
        auxstr = ''
        for char in str:
            if char == ',':
                lst[0] = int(auxstr)
                tupla = tuple(lst)
                auxstr = ''
            elif char == '\n':
                lst[1] = int(auxstr)
                tupla = tuple(lst)
                Paux2.append(tupla)
                break
            else:
                auxstr += char

    return Paux2
