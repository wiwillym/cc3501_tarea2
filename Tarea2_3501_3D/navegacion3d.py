from utils3d import *
from boat3d import Boat
from PyOpenGLtoolbox import *

filename = open("trayectoria3d.txt", "r")
Pauxstr = []
for line in filename:
    Pauxstr.append(line)
Pauxstr[len(Pauxstr) - 1] += '\n'
Points = txttovector(Pauxstr)
light_blue = rgb(0, 102, 204)
green = rgb(0, 153, 80)
white = rgb(255, 255, 255)

def reshape_window_perspective(w, h, near, far, fov):
    """
    Crea la ventana con una proyección en perspectiva.
    """
    h = max(h, 1)
    glLoadIdentity()

    #   Crea el viewport
    glViewport(0, 0, int(w), int(h))
    glMatrixMode(GL_PROJECTION)

    #   Proyección en perspectiva
    gluPerspective(fov, float(w) / float(h), near, far)

    #   Setea el modo de la cámara
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def drawBackground():
    #   Green Background
    glBegin(GL_QUADS)

    global light_blue
    glColor3fv(light_blue)

    glVertex2fv([800, 600])
    glVertex2fv([0, 600])
    glVertex2fv([0, 0])
    glVertex2fv([800, 0])

    glEnd()

    #   Shapes for making the lake
    #   n_1
    glBegin(GL_TRIANGLES)

    global green
    glColor3fv(green)

    glVertex2fv([800, 600])
    glVertex2fv([500, 570])
    glVertex2fv([300, 600])

    glEnd()

    #   n_2
    glBegin(GL_QUADS)

    glVertex2fv([800, 600])
    glVertex2fv([800, 585])
    glVertex2fv([500, 585])
    glVertex2fv([500, 600])

    glEnd()

    #   n_3
    glBegin(GL_TRIANGLES)

    glVertex2fv([550, 600])
    glVertex2fv([0, 575])
    glVertex2fv([0, 600])

    glEnd()

    #   n_4
    glBegin(GL_TRIANGLES)

    glVertex2fv([70, 600])
    glVertex2fv([0, 470])
    glVertex2fv([0, 600])

    glEnd()

    #   n_5
    glBegin(GL_TRIANGLES)

    glVertex2fv([30, 600])
    glVertex2fv([0, 150])
    glVertex2fv([0, 600])

    glEnd()

    #   n_6
    glBegin(GL_TRIANGLES)

    glVertex2fv([0, 350])
    glVertex2fv([70, 0])
    glVertex2fv([0, 0])

    glEnd()

    #   n_7
    glBegin(GL_TRIANGLES)

    glVertex2fv([0, 200])
    glVertex2fv([150, 100])
    glVertex2fv([0, 0])

    glEnd()

    #   n_8
    glBegin(GL_QUADS)

    glVertex2fv([0, 100])
    glVertex2fv([150, 100])
    glVertex2fv([150, 0])
    glVertex2fv([0, 0])

    glEnd()

    #   n_9
    glBegin(GL_TRIANGLES)

    glVertex2fv([150, 100])
    glVertex2fv([150, 0])
    glVertex2fv([180, 0])

    glEnd()

    #   n_10
    glBegin(GL_QUADS)

    glVertex2fv([425, 0])
    glVertex2fv([425, 25])
    glVertex2fv([800, 25])
    glVertex2fv([800, 0])

    glEnd()

    #   n_11
    glBegin(GL_TRIANGLES)

    glVertex2fv([410, 0])
    glVertex2fv([425, 25])
    glVertex2fv([425, 0])

    glEnd()

    #   n_12
    glBegin(GL_TRIANGLES)

    glVertex2fv([425, 25])
    glVertex2fv([800, 25])
    glVertex2fv([800, 50])

    glEnd()

    #   n_13
    glBegin(GL_TRIANGLES)

    glVertex2fv([450, 0])
    glVertex2fv([700, 100])
    glVertex2fv([700, 0])

    glEnd()

    #   n_14
    glBegin(GL_QUADS)

    glVertex2fv([670, 0])
    glVertex2fv([670, 180])
    glVertex2fv([800, 180])
    glVertex2fv([800, 0])

    glEnd()

    #   n_15
    glBegin(GL_TRIANGLES)

    glVertex2fv([670, 180])
    glVertex2fv([645, 180])
    glVertex2fv([670, 0])

    glEnd()

    #   n_16
    glBegin(GL_TRIANGLES)

    glVertex2fv([645, 180])
    glVertex2fv([670, 180])
    glVertex2fv([670, 220])

    glEnd()

    #   n_17
    glBegin(GL_QUADS)

    glVertex2fv([670, 220])
    glVertex2fv([740, 220])
    glVertex2fv([740, 180])
    glVertex2fv([670, 180])

    glEnd()

    #   n_18
    glBegin(GL_QUADS)

    glVertex2fv([740, 180])
    glVertex2fv([800, 180])
    glVertex2fv([800, 210])
    glVertex2fv([740, 210])

    glEnd()

    #   n_19
    glBegin(GL_TRIANGLES)

    glVertex2fv([740, 220])
    glVertex2fv([740, 210])
    glVertex2fv([800, 210])

    glEnd()

    #   Border of the island
    #   Lower left side
    glBegin(GL_QUADS)

    glVertex2fv([307, 257])
    glVertex2fv([332, 257])
    glVertex2fv([347, 313])
    glVertex2fv([322, 313])

    glEnd()

    glBegin(GL_QUADS)

    glVertex2fv([307, 257])
    glVertex2fv([332, 257])
    glVertex2fv([326, 250])
    glVertex2fv([320, 250])

    glEnd()

    #   Middle
    glBegin(GL_QUADS)

    glVertex2fv([321, 310])
    glVertex2fv([324, 320])
    glVertex2fv([405, 320])
    glVertex2fv([402, 310])

    glEnd()

    #   Lower right side
    glBegin(GL_QUADS)

    glVertex2fv([363, 257])
    glVertex2fv([388, 257])
    glVertex2fv([403, 313])
    glVertex2fv([378, 313])

    glEnd()

    glBegin(GL_QUADS)

    glVertex2fv([388, 257])
    glVertex2fv([363, 257])
    glVertex2fv([370, 247])
    glVertex2fv([380, 247])

    glEnd()

    #   Upper left side
    glBegin(GL_QUADS)

    glVertex2fv([323, 317])
    glVertex2fv([347, 317])
    glVertex2fv([362, 363])
    glVertex2fv([338, 363])

    glEnd()

    #   Upper right side
    glBegin(GL_QUADS)

    glVertex2fv([380, 317])
    glVertex2fv([404, 317])
    glVertex2fv([419, 363])
    glVertex2fv([395, 363])

    glEnd()

    #   Head of the island n_1
    glBegin(GL_QUADS)

    glVertex2fv([338, 363])
    glVertex2fv([362, 363])
    glVertex2fv([382, 383])
    glVertex2fv([358, 383])

    glEnd()

    #   Head of the island n_2
    glBegin(GL_QUADS)

    glVertex2fv([419, 363])
    glVertex2fv([395, 363])
    glVertex2fv([382, 383])
    glVertex2fv([406, 383])

    glEnd()

    #   Head of the island n_3

    glBegin(GL_QUADS)

    glVertex2fv([358, 383])
    glVertex2fv([406, 383])
    glVertex2fv([395, 400])
    glVertex2fv([375, 400])

    glEnd()

    #   Head of the island n_4

    glBegin(GL_QUADS)

    glVertex2fv([375, 400])
    glVertex2fv([395, 400])
    glVertex2fv([390, 405])
    glVertex2fv([385, 405])

    glEnd()

    #   Head of the island n_5
    glBegin(GL_TRIANGLES)

    glVertex2fv([350, 370])
    glVertex2fv([410, 370])
    glVertex2fv([380, 400])

    glEnd()

    """
    Se agregarán nubes para que se vea mas bonito
    """

    #   Cloud_1
    glBegin(GL_QUADS)

    global white
    glColor3fv(white)

    glVertex3fv([150, 600, 200])
    glVertex3fv([150, 600, 230])
    glVertex3fv([300, 600, 230])
    glVertex3fv([300, 600, 200])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([200, 600, 230])
    glVertex3fv([200, 600, 245])
    glVertex3fv([250, 600, 245])
    glVertex3fv([250, 600, 230])

    glEnd()

    #   Cloud_2
    glBegin(GL_QUADS)

    glVertex3fv([330, 600, 140])
    glVertex3fv([330, 600, 170])
    glVertex3fv([400, 600, 170])
    glVertex3fv([400, 600, 140])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([385, 600, 150])
    glVertex3fv([385, 600, 130])
    glVertex3fv([450, 600, 130])
    glVertex3fv([450, 600, 150])

    glEnd()

    #   Cloud_3
    glBegin(GL_QUADS)

    glVertex3fv([600, 600, 400])
    glVertex3fv([600, 600, 460])
    glVertex3fv([750, 600, 460])
    glVertex3fv([750, 600, 400])

    glEnd()

    #   Cloud_4
    glBegin(GL_QUADS)

    glVertex3fv([470, 600, 300])
    glVertex3fv([470, 600, 330])
    glVertex3fv([610, 600, 330])
    glVertex3fv([610, 600, 300])

    glEnd()

    #   Cloud_5
    glBegin(GL_QUADS)

    glVertex3fv([150, 0, 200])
    glVertex3fv([150, 0, 230])
    glVertex3fv([300, 0, 230])
    glVertex3fv([300, 0, 200])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([200, 0, 230])
    glVertex3fv([200, 0, 245])
    glVertex3fv([250, 0, 245])
    glVertex3fv([250, 0, 230])

    glEnd()

    #   Cloud_6
    glBegin(GL_QUADS)

    glVertex3fv([330, 0, 140])
    glVertex3fv([330, 0, 170])
    glVertex3fv([400, 0, 170])
    glVertex3fv([400, 0, 140])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([385, 0, 150])
    glVertex3fv([385, 0, 130])
    glVertex3fv([450, 0, 130])
    glVertex3fv([450, 0, 150])

    glEnd()

    #   Cloud_7
    glBegin(GL_QUADS)

    glVertex3fv([600, 0, 400])
    glVertex3fv([600, 0, 460])
    glVertex3fv([750, 0, 460])
    glVertex3fv([750, 0, 400])

    glEnd()

    #   Cloud_8
    glBegin(GL_QUADS)

    glVertex3fv([470, 0, 300])
    glVertex3fv([470, 0, 330])
    glVertex3fv([610, 0, 330])
    glVertex3fv([610, 0, 300])

    glEnd()

    #   C9
    glBegin(GL_QUADS)

    glVertex3fv([0, 200, 300])
    glVertex3fv([0, 200, 360])
    glVertex3fv([0, 300, 360])
    glVertex3fv([0, 300, 300])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([0, 280, 330])
    glVertex3fv([0, 280, 370])
    glVertex3fv([0, 330, 370])
    glVertex3fv([0, 330, 330])

    glEnd()

    #   Cloud_10
    glBegin(GL_QUADS)

    glVertex3fv([0, 0, 100])
    glVertex3fv([0, 0, 70])
    glVertex3fv([0, 100, 70])
    glVertex3fv([0, 100, 100])

    glEnd()

    #   Cloud_11
    glBegin(GL_QUADS)

    glVertex3fv([0, 450, 200])
    glVertex3fv([0, 450, 260])
    glVertex3fv([0, 570, 260])
    glVertex3fv([0, 570, 200])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([0, 500, 220])
    glVertex3fv([0, 500, 180])
    glVertex3fv([0, 400, 180])
    glVertex3fv([0, 400, 220])

    glEnd()

    #   Cloud_12
    glBegin(GL_QUADS)

    glVertex3fv([800, 200, 300])
    glVertex3fv([800, 200, 360])
    glVertex3fv([800, 300, 360])
    glVertex3fv([800, 300, 300])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([800, 280, 330])
    glVertex3fv([800, 280, 370])
    glVertex3fv([800, 330, 370])
    glVertex3fv([800, 330, 330])

    glEnd()

    #   Cloud_13
    glBegin(GL_QUADS)

    glVertex3fv([800, 0, 100])
    glVertex3fv([800, 0, 70])
    glVertex3fv([800, 100, 70])
    glVertex3fv([800, 100, 100])

    glEnd()

    #   Cloud_14
    glBegin(GL_QUADS)

    glVertex3fv([800, 450, 200])
    glVertex3fv([800, 450, 260])
    glVertex3fv([800, 570, 260])
    glVertex3fv([800, 570, 200])

    glEnd()

    glBegin(GL_QUADS)

    glVertex3fv([800, 500, 220])
    glVertex3fv([800, 500, 180])
    glVertex3fv([800, 400, 180])
    glVertex3fv([800, 400, 220])

    glEnd()

Width = 800
Height = 600
FOV = 60
FPS = 60

init_pygame(Width, Height, 'Tarea2b', centered_window=True)
init_gl(transparency=False, materialcolor=False, normalized=True, perspectivecorr=True, antialiasing=True, depth=True, smooth=True, verbose=True, version=True)
reshape_window_perspective(w=Width, h=Height, near=1, far=2000, fov=FOV)
clock = pygame.time.Clock()

boat = Boat([0, 0, 0])

CAMARA_POS = [0, 0, 200]  # Donde estoy (x,y,z)
CAMARA_CENTRO = [600, 500, 0]  # Dónde apunto (x,y,z)
CAMARA_NORMAL = [0, 0, 1]  # La camara está parada normal al eje z

#   Medimos tiempo inicial
t0 = pygame.time.get_ticks()
CRS = Boat.calcularTrayectoria(Boat, Points)
instante = 0
maxInstante = len(CRS) - 1
run = True

#   Sun
glLightfv(GL_LIGHT0, GL_POSITION, [400.0, 300.0, 1000.0, 1.0])
glLightfv(GL_LIGHT0, GL_AMBIENT, [0.6, 0.6, 0.6, 1.0])
glLightfv(GL_LIGHT0, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
glLightfv(GL_LIGHT0, GL_DIFFUSE, [1.0, 1.0, 1.0, 1.0])
glEnable(GL_LIGHT0)

#   Ambient Light 1
glLightfv(GL_LIGHT1, GL_POSITION, [400.0, 0.0, 500.0, 1.0])
glLightfv(GL_LIGHT1, GL_AMBIENT, [0.2, 0.2, 0.2, 1.0])
glLightfv(GL_LIGHT1, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
glEnable(GL_LIGHT1)

#   Ambient Light 2
glLightfv(GL_LIGHT2, GL_POSITION, [0.0, 300.0, 500.0, 1.0])
glLightfv(GL_LIGHT2, GL_AMBIENT, [0.2, 0.2, 0.2, 1.0])
glLightfv(GL_LIGHT2, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
glEnable(GL_LIGHT2)

#   Ambient Light 3
glLightfv(GL_LIGHT3, GL_POSITION, [800.0, 300.0, 500.0, 1.0])
glLightfv(GL_LIGHT3, GL_AMBIENT, [0.2, 0.2, 0.2, 1.0])
glLightfv(GL_LIGHT3, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
glEnable(GL_LIGHT3)

#   Ambient Light 4
glLightfv(GL_LIGHT4, GL_POSITION, [400.0, 600.0, 500.0, 1.0])
glLightfv(GL_LIGHT4, GL_AMBIENT, [0.2, 0.2, 0.2, 1.0])
glLightfv(GL_LIGHT4, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
glEnable(GL_LIGHT4)

glEnable(GL_LIGHTING)

while run:

    glClearColor(0.196078, 0.6, 0.8, 1.0)

    if instante == maxInstante:
        instante = 0

    clock.tick(FPS)

    clear_buffer()

    glLoadIdentity()

    gluLookAt(CAMARA_POS[0], CAMARA_POS[1], CAMARA_POS[2],
              CAMARA_CENTRO[0], CAMARA_CENTRO[1], CAMARA_CENTRO[2],
              CAMARA_NORMAL[0], CAMARA_NORMAL[1], CAMARA_NORMAL[2])

    for event in pygame.event.get():
        if event.type == QUIT:
            run = False
        if event.type == KEYDOWN:
            if event.key == K_1:
                CAMARA_POS = [0, 0, 200]
                CAMARA_CENTRO = [600, 500, 0]
                CAMARA_NORMAL = [0, 0, 1]
                gluLookAt(CAMARA_POS[0], CAMARA_POS[1], CAMARA_POS[2],
                          CAMARA_CENTRO[0], CAMARA_CENTRO[1], CAMARA_CENTRO[2],
                          CAMARA_NORMAL[0], CAMARA_NORMAL[1], CAMARA_NORMAL[2])
            if event.key == K_2:
                CAMARA_POS = [800, 0, 200]
                CAMARA_CENTRO = [200, 500, 0]
                CAMARA_NORMAL = [0, 0, 1]
                gluLookAt(CAMARA_POS[0], CAMARA_POS[1], CAMARA_POS[2],
                          CAMARA_CENTRO[0], CAMARA_CENTRO[1], CAMARA_CENTRO[2],
                          CAMARA_NORMAL[0], CAMARA_NORMAL[1], CAMARA_NORMAL[2])
            if event.key == K_3:
                CAMARA_POS = [800, 600, 200]
                CAMARA_CENTRO = [200, 100, 0]
                CAMARA_NORMAL = [0, 0, 1]
                gluLookAt(CAMARA_POS[0], CAMARA_POS[1], CAMARA_POS[2],
                          CAMARA_CENTRO[0], CAMARA_CENTRO[1], CAMARA_CENTRO[2],
                          CAMARA_NORMAL[0], CAMARA_NORMAL[1], CAMARA_NORMAL[2])
            if event.key == K_4:
                CAMARA_POS = [0, 600, 200]
                CAMARA_CENTRO = [600, 100, 0]
                CAMARA_NORMAL = [0, 0, 1]
                gluLookAt(CAMARA_POS[0], CAMARA_POS[1], CAMARA_POS[2],
                          CAMARA_CENTRO[0], CAMARA_CENTRO[1], CAMARA_CENTRO[2],
                          CAMARA_NORMAL[0], CAMARA_NORMAL[1], CAMARA_NORMAL[2])

    glDisable(GL_LIGHTING)
    drawBackground()
    glEnable(GL_LIGHTING)

    glMaterialfv(GL_FRONT, GL_AMBIENT, [0.0, 0.0, 0.0])
    glMaterialfv(GL_FRONT, GL_DIFFUSE, [0.01, 0.01, 0.01])
    glMaterialfv(GL_FRONT, GL_SPECULAR, [0.50, 0.50, 0.50])
    glMaterialfv(GL_FRONT, GL_SHININESS, [32.0])
    SpaceDrawing(CRS, Points)

    boat.mover(instante, maxInstante)

    boat.dibujar(CRS[instante + 1][1], CRS[instante - 1][1], CRS[instante + 1][0], CRS[instante - 1][0])

    pygame.display.flip() # Refresh screen

    pygame.time.wait(int(1000 / 200)) #  200 fps

    instante += 1
