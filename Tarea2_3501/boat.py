from OpenGL.GL import *
import numpy as np
import math
from pyworkspace.Tarea2_3501.utils import *
filename = open("trayectoria.txt", "r")
Pauxstr = []
for line in filename:
    Pauxstr.append(line)
Pauxstr[len(Pauxstr) - 1] += '\n'
Points = txttovector(Pauxstr)


class Boat:
    def __init__(self, p):
        self.p = np.array(p) #  posicion
        self.trayectoria = self.calcularTrayectoria(Points)

    def calcularTrayectoria(self, P):
        CRS = CatmullRomChain([P[0], P[1], P[2], P[3]])
        for p in range(len(P)):
            if p + 4 <= len(P) - 1:
                Paux = [P[p + 1], P[p + 2], P[p + 3], P[p + 4]]
                CRSaux = CatmullRomChain(Paux)
                CRS = np.concatenate((CRS, CRSaux))
            else:
                break
        return CRS

    def mover(self, instante, maxInstante):
        self.calcularTrayectoria(Points)
        self.p = np.array([self.trayectoria[instante][0], self.trayectoria[instante][1]])

    def dibujar(self, y2, y1, x2, x1):

        glPushMatrix()

        glTranslatef(self.p[0], self.p[1], 0.0)

        glRotatef(math.degrees(np.arctan((y2 - y1) / (x2 - x1))), 0, 0, 1)

        #   Base n_1
        glColor3fv(rgb(51, 25, 0))

        glBegin(GL_POLYGON)

        glVertex2fv([-40, 0])
        glVertex2fv([-25, -25])
        glVertex2fv([25, -25])
        glVertex2fv([40, 0])
        glVertex2fv([25, 25])
        glVertex2fv([-25, 25])

        glEnd()

        #   Base n_2
        glColor3fv(rgb(102, 51, 0))

        glBegin(GL_QUADS)

        glVertex2fv([-25, -15])
        glVertex2fv([-25, 15])
        glVertex2fv([25, 15])
        glVertex2fv([25, -15])

        glEnd()

        #   Sail
        glColor3fv(rgb(255, 255, 255))

        glBegin(GL_TRIANGLES)

        glVertex2fv([0, 0])
        glVertex2fv([0, 65])
        glVertex2fv([25, 0])

        glEnd()

        glColor3fv(rgb(224, 224, 224))

        glBegin(GL_TRIANGLES)

        glVertex2fv([0, 0])
        glVertex2fv([0, 85])
        glVertex2fv([-15, 0])

        glEnd()

        glPopMatrix()
