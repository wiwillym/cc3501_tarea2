from pyworkspace.Tarea2_3501.utils import *
from pyworkspace.Tarea2_3501.boat import Boat

filename = open("trayectoria.txt", "r")
Pauxstr = []
for line in filename:
    Pauxstr.append(line)
Pauxstr[len(Pauxstr) - 1] += '\n'
Points = txttovector(Pauxstr)
light_blue = rgb(0, 102, 204)
green = rgb(0, 153, 80)


def drawBackground():

    #   Green Background
    glBegin(GL_QUADS)

    global light_blue
    glColor3fv(light_blue)

    glVertex2fv([800, 600])
    glVertex2fv([0, 600])
    glVertex2fv([0, 0])
    glVertex2fv([800, 0])

    glEnd()

    #   Shapes for making the lake
    #   n_1
    glBegin(GL_TRIANGLES)

    global green
    glColor3fv(green)

    glVertex2fv([800, 600])
    glVertex2fv([500, 570])
    glVertex2fv([300, 600])

    glEnd()

    #   n_2
    glBegin(GL_QUADS)

    glVertex2fv([800, 600])
    glVertex2fv([800, 585])
    glVertex2fv([500, 585])
    glVertex2fv([500, 600])

    glEnd()

    #   n_3
    glBegin(GL_TRIANGLES)

    glVertex2fv([550, 600])
    glVertex2fv([0, 575])
    glVertex2fv([0, 600])

    glEnd()

    #   n_4
    glBegin(GL_TRIANGLES)

    glVertex2fv([70, 600])
    glVertex2fv([0, 470])
    glVertex2fv([0, 600])

    glEnd()

    #   n_5
    glBegin(GL_TRIANGLES)

    glVertex2fv([30, 600])
    glVertex2fv([0, 150])
    glVertex2fv([0, 600])

    glEnd()

    #   n_6
    glBegin(GL_TRIANGLES)

    glVertex2fv([0, 350])
    glVertex2fv([70, 0])
    glVertex2fv([0, 0])

    glEnd()

    #   n_7
    glBegin(GL_TRIANGLES)

    glVertex2fv([0, 200])
    glVertex2fv([150, 100])
    glVertex2fv([0, 0])

    glEnd()

    #   n_8
    glBegin(GL_QUADS)

    glVertex2fv([0, 100])
    glVertex2fv([150, 100])
    glVertex2fv([150, 0])
    glVertex2fv([0, 0])

    glEnd()

    #   n_9
    glBegin(GL_TRIANGLES)

    glVertex2fv([150, 100])
    glVertex2fv([150, 0])
    glVertex2fv([180, 0])

    glEnd()

    #   n_10
    glBegin(GL_QUADS)

    glVertex2fv([425, 0])
    glVertex2fv([425, 25])
    glVertex2fv([800, 25])
    glVertex2fv([800, 0])

    glEnd()

    #   n_11
    glBegin(GL_TRIANGLES)

    glVertex2fv([410, 0])
    glVertex2fv([425, 25])
    glVertex2fv([425, 0])

    glEnd()

    #   n_12
    glBegin(GL_TRIANGLES)

    glVertex2fv([425, 25])
    glVertex2fv([800, 25])
    glVertex2fv([800, 50])

    glEnd()

    #   n_13
    glBegin(GL_TRIANGLES)

    glVertex2fv([450, 0])
    glVertex2fv([700, 100])
    glVertex2fv([700, 0])

    glEnd()

    #   n_14
    glBegin(GL_QUADS)

    glVertex2fv([670, 0])
    glVertex2fv([670, 180])
    glVertex2fv([800, 180])
    glVertex2fv([800, 0])

    glEnd()

    #   n_15
    glBegin(GL_TRIANGLES)

    glVertex2fv([670, 180])
    glVertex2fv([645, 180])
    glVertex2fv([670, 0])

    glEnd()

    #   n_16
    glBegin(GL_TRIANGLES)

    glVertex2fv([645, 180])
    glVertex2fv([670, 180])
    glVertex2fv([670, 220])

    glEnd()

    #   n_17
    glBegin(GL_QUADS)

    glVertex2fv([670, 220])
    glVertex2fv([740, 220])
    glVertex2fv([740, 180])
    glVertex2fv([670, 180])

    glEnd()

    #   n_18
    glBegin(GL_QUADS)

    glVertex2fv([740, 180])
    glVertex2fv([800, 180])
    glVertex2fv([800, 210])
    glVertex2fv([740, 210])

    glEnd()

    #   n_19
    glBegin(GL_TRIANGLES)

    glVertex2fv([740, 220])
    glVertex2fv([740, 210])
    glVertex2fv([800, 210])

    glEnd()

    #   Border of the island
    #   Lower left side
    glBegin(GL_QUADS)

    glVertex2fv([307, 257])
    glVertex2fv([332, 257])
    glVertex2fv([347, 313])
    glVertex2fv([322, 313])

    glEnd()

    glBegin(GL_QUADS)

    glVertex2fv([307, 257])
    glVertex2fv([332, 257])
    glVertex2fv([326, 250])
    glVertex2fv([320, 250])

    glEnd()

    #   Middle
    glBegin(GL_QUADS)

    glVertex2fv([321, 310])
    glVertex2fv([324, 320])
    glVertex2fv([405, 320])
    glVertex2fv([402, 310])

    glEnd()

    #   Lower right side
    glBegin(GL_QUADS)

    glVertex2fv([363, 257])
    glVertex2fv([388, 257])
    glVertex2fv([403, 313])
    glVertex2fv([378, 313])

    glEnd()

    glBegin(GL_QUADS)

    glVertex2fv([388, 257])
    glVertex2fv([363, 257])
    glVertex2fv([370, 247])
    glVertex2fv([380, 247])

    glEnd()

    #   Upper left side
    glBegin(GL_QUADS)

    glVertex2fv([323, 317])
    glVertex2fv([347, 317])
    glVertex2fv([362, 363])
    glVertex2fv([338, 363])

    glEnd()

    #   Upper right side
    glBegin(GL_QUADS)

    glVertex2fv([380, 317])
    glVertex2fv([404, 317])
    glVertex2fv([419, 363])
    glVertex2fv([395, 363])

    glEnd()

    #   Head of the island n_1
    glBegin(GL_QUADS)

    glVertex2fv([338, 363])
    glVertex2fv([362, 363])
    glVertex2fv([382, 383])
    glVertex2fv([358, 383])

    glEnd()

    #   Head of the island n_2
    glBegin(GL_QUADS)

    glVertex2fv([419, 363])
    glVertex2fv([395, 363])
    glVertex2fv([382, 383])
    glVertex2fv([406, 383])

    glEnd()

    #   Head of the island n_3

    glBegin(GL_QUADS)

    glVertex2fv([358, 383])
    glVertex2fv([406, 383])
    glVertex2fv([395, 400])
    glVertex2fv([375, 400])

    glEnd()

    #   Head of the island n_4

    glBegin(GL_QUADS)

    glVertex2fv([375, 400])
    glVertex2fv([395, 400])
    glVertex2fv([390, 405])
    glVertex2fv([385, 405])

    glEnd()

    #   Head of the island n_5
    glBegin(GL_TRIANGLES)

    glVertex2fv([350, 370])
    glVertex2fv([410, 370])
    glVertex2fv([380, 400])

    glEnd()


def init():
    Width = 800
    Height = 600

    pygame.init()
    pygame.display.set_mode((Width, Height), OPENGL | DOUBLEBUF)
    pygame.display.set_caption("Tarea 2")

    #   inicializar opengl

    glViewport(0, 0, Width, Height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0.0, Width, 0.0, Height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    #   Definir variables de openGL
    glClearColor(0.0, 0.0, 0.0, 0.0) #  color de fondo
    glShadeModel(GL_SMOOTH)
    glClearDepth(1.0)


boat = Boat([0, 0, 0])

if __name__ == '__main__':
    init()
    #   Medimos tiempo inicial
    t0 = pygame.time.get_ticks()
    CRS = Boat.calcularTrayectoria(Boat, Points)
    instante = 0
    maxInstante = len(CRS) - 1
    run = True
    while run:
        if instante == maxInstante:
            instante = 0

        #   Control del tiempo
        t1 = pygame.time.get_ticks() #  tiempo actual
        dt = (t1 - t0)
        t0 = t1

        for event in pygame.event.get():
            if event.type == QUIT:
                run = False

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) #    Clean buffers

        drawBackground()

        SpaceDrawing(CRS, Points)

        boat.mover(instante, maxInstante)

        boat.dibujar(CRS[instante + 1][1], CRS[instante - 1][1], CRS[instante + 1][0], CRS[instante - 1][0])

        pygame.display.flip() # Refresh screen
        pygame.time.wait(int(1000 / 200)) #  200 fps

        instante += 1

    pygame.quit()
